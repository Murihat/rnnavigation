# First try react-navigation 3x (react-native 0.60)
## Installation

> Install the react-navigation package in your React Native project.
```
https://reactnavigation.org/docs/en/getting-started.html

npm install react-navigation
npm install react-native-gesture-handler react-native-reanimated
```
> To finalize installation for Android, be sure to make the necessary modifications to MainActivity.java:
```
package com.reactnavigation.example;

import com.facebook.react.ReactActivity;
+ import com.facebook.react.ReactActivityDelegate;
+ import com.facebook.react.ReactRootView;
+ import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

public class MainActivity extends ReactActivity {

  @Override
  protected String getMainComponentName() {
    return "Example";
  }

+  @Override
+  protected ReactActivityDelegate createReactActivityDelegate() {
+    return new ReactActivityDelegate(this, getMainComponentName()) {
+      @Override
+      protected ReactRootView createRootView() {
+       return new RNGestureHandlerEnabledRootView(MainActivity.this);
+      }
+    };
+  }
}
```