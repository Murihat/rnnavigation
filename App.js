import React, { Component } from 'react'
import { Button } from 'react-native'
import DrawerNavigation from './src/navigation/DrawerNavigation';

class App extends Component {
  render() {
    return <DrawerNavigation />;
  }
}
export default App;