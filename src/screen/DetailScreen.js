import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'

export default class DetailScreen extends Component {
  
    render() {
      const { navigation } = this.props;
      const name = navigation.getParam('params');

        return (
        <View style={styles.container}>
            <Text style={styles.text}> Detail Pages {name} </Text>
        </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text : {
    fontSize: 30,
    textAlign: 'center'
  },
})