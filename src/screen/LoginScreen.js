import React, { Component } from 'react'
import { Text, StyleSheet, View, Button, Dimensions, TextInput } from 'react-native'

const {height, width} = Dimensions.get('window');

export default class LoginScreen extends Component {
  // static navigationOptions = ({ navigation }) => {
  //   return { 
  //       headerStyle: {
  //         backgroundColor: '#0091EA',
  //         height: 40
  //       },
  //       headerTintColor: '#fff',
  //       title: 'Login',
  //   };
  // };

  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
    this.state = {email: '', password: '', action: true};
    this.hide = {action: true};
  }

  _onPressButton = async () => {
    // gagal
    // this.setHide({ action: !this.hide.action });
   
    this.hide.action = false;
    // berhasil
    // this.setState({ action: !this.hide.action });
    console.log("aaaaaaaaaaaaaaaa", this.hide )
  }


  render() {
      return (
      <View style={styles.container}>
        <View style={styles.row}>
          <View style={styles.columnFirst}>
            <Text> Murihat </Text>
            <TextInput
              style={{height: 40,borderColor: 'gray',borderWidth: 1, marginBottom: 5}}
              placeholder="Email Address..."
              onChangeText={(email) => this.setState({email})}
              value={this.state.email} 
              autoCompleteType="email"
            />     

            <View style={styles.password}>
              <TextInput
                style={{height: 40,borderColor: 'gray',borderWidth: 1, marginBottom: 5, flex: 1}}
                placeholder="Password..."
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                autoCompleteType="password"
                secureTextEntry={this.hide.action}
              />       
              <Button title="Show Password" onPress={this._onPressButton} style={{flex: 1}}/>    
            </View>       
               
            <Button title="Login" onPress={() => this.props.navigation.navigate('Home')}/>
          </View>
        </View>
        <View>
          <View style={styles.columnTwo}>

          </View>
        </View>
      </View>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#cccccc",
  },

  row: {
    height: "50%",
    backgroundColor: "#87CEEB",
    justifyContent: "center",
  },

  columnFirst: {
    marginHorizontal: 18
  },

  password: {
    flexDirection: 'row'
  }

  // columnTwo: {
  //   height: "50%",
  //   backgroundColor: "#FFFFFF",
  // }

})