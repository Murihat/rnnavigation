import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'

export default class AccountScreen extends Component {
    render() {
        return (
        <View style={styles.container}>
            <Text style={styles.text}> Account Pages </Text>
        </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text : {
    fontSize: 30,
  },
})