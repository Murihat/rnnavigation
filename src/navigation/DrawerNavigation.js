import React, { Component } from 'react';
import { Button } from 'react-native'
import {
    createAppContainer,
    createDrawerNavigator,
    createBottomTabNavigator,
    createStackNavigator,
    createSwitchNavigator
  } from 'react-navigation';

import Home from '../screen/HomeScreen'
import Market from '../screen/MarketScreen'
import Account from '../screen/AccountScreen'
import Detail from '../screen/DetailScreen'
import LoginScreen from '../screen/LoginScreen';


const TabNavigator = createBottomTabNavigator(
    {
        Home,
        Market,
        Account
    },
    {
      navigationOptions: ({ navigation }) => {
        const { routeName } = navigation.state.routes[navigation.state.index];
        return {
          headerTitle: routeName,
          headerStyle: {
            backgroundColor: '#0091EA',
            height: 45
          },
          headerTintColor: '#fff',
        };
      }
    }
);

const HomeStackNavigator = createStackNavigator(
    {
        Home: screen = TabNavigator
    },
);

const LoginStackNavigator = createStackNavigator(
    {
        Login: screen = LoginScreen,
    },
    {
        defaultNavigationOptions: {
            header: null,
        },
    }
);

const AppDrawerNavigator = createDrawerNavigator({
    Home: {
      screen: HomeStackNavigator
    },
    Login: {
        screen: LoginStackNavigator
    }
});

const AppSwitchNavigator = createSwitchNavigator({
    Login: { screen: LoginStackNavigator },
    Home: { screen: HomeStackNavigator }
});

//kenapa harus sama drawer sama stacknya anjay
  
export default createAppContainer(AppSwitchNavigator);